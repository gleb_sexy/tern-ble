# Tern Ble

Remix of [Tern](https://github.com/rschenk/tern) keyboard.

![Photo of Tern Ble keyboard at sunset](images/sunset.jpg)

List of changes:

* Support for an on/off switch, battery connector (JST PH 2.0mm) and XIAO Ble battery pogo pin
* Slightly different switch footprint with tight holes, switches are held firmly without a plate
* Pinky column is a bit closer to the ring finger column so the gap is thinner and looks consistent
* PCB outline designed from the ground up
* Beautiful and fun silkscreen design showcasing the typical behavior of these birds

Features:

* 30 keys, Hummingbird layout
* Choc hotswap switches 
* CFX keycap spacing (MBK/choc spaced keycaps won't work)
* XIAO Ble controller
* 100 mAh battery
* 3D printed case

![Photo of Tern Ble](images/top_marked.jpg)
![Photo of Tern Ble PCB](images/pcb.jpg)

## Case

The PCB outline is completely different from the original Tern so anything designed for Tern original isn't gonna work. You can use SVG files provided in this repo to create your own case with the desired design. 

I created a simple dish-shaped case design that is held by friction but it warps the PCB and doesn't lay flat, so better add some holders for nuts or heat inserts.

The top case with an integrated plate works well, you can 3D print it safely. It is designed to only fit a small 100 mAh battery under the controller (10x23mm in size). With this battery a thin wire is soldered to the battery pin instead of a pogo pin.

![Photo of Tern Ble from the underside](images/backside.jpg)

## Firmware

Original Tern firmware works fine with adjusted name and controller. You can find it at in this repo and at [this commit](https://github.com/glebsexy/zmk-config/tree/911d1b0bc5af01ccc52c6439ff8a922555e8d626) in my zmk config repo. For customization inspiration take a peek at [my ZMK config](https://github.com/glebsexy/zmk-config).

## Build Guide

[Refer to Tern build guide](https://github.com/rschenk/tern/blob/main/docs/build_guide.md)

## Thanks

* [Tern](https://github.com/rschenk/tern/) for the original Tern keyboard
* [Hummingbird](https://github.com/PJE66/hummingbird) for the original idea and matrix
* [Rufous](https://github.com/jcmkk3/trochilidae) for the basis of the Ergogen config and routing

![Photo of Tern Ble](images/front_marked.jpg)